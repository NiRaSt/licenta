﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public class MakeTile : MonoBehaviour {

    //the coordonates of where the player will be dropped
    public Vector3Int position;
    //the tilemap where the tiles will pe set
    public Tilemap tileMap;
    public Tilemap ground;
    //the sprites used for the map
    public Tile groundTile;
    public Tile waterTile;
    public Tile emptyTile;
    //the width and height of the cave, NOT the actual image
    static int width = 78, height = 32;
    //the cell map of the cave, where TRUE is a wall
    Boolean[,] cellmap = new Boolean[width, height];
    //the player
    public GameObject player;

    //the Death Limit and Birth Limit of the map
    static int deathLimit = 4;
    static int birthLimit = 5;

    //the number of steps that the cell map goes trhough
    static int numberOfSteps = 3;

    //the chance of a cell to start alive
    static float chanceToStartAlive = 0.45f;

    //the function that initialises the cell map
    public static Boolean[,] initialiseMap()
    {
        Boolean[,] map = new Boolean[width, height];
        System.Random rng = new System.Random();

        for (int x = 0; x < width; x++)
        {

            for (int y = 0; y < height; y++)
            {
                double random = rng.NextDouble();
                if (random < chanceToStartAlive)
                {
                    map[x, y] = true;
                }
            }
        }
        return map;
    }

    //the function that does the simulation step
    public static Boolean[,] doSimulationStep(Boolean[,] oldMap)
    {
        Boolean[,] newMap = new Boolean[width, height];
        //Loop over each row and column of the map
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int nbs = countAliveNeighbours(oldMap, x, y);
                //The new value is based on our simulation rules
                //First, if a cell is alive but has too few neighbours, kill it.
                if (oldMap[x, y])
                {
                    if (nbs < deathLimit)
                    {
                        newMap[x, y] = false;
                    }
                    else
                    {
                        newMap[x, y] = true;
                    }
                } //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                else
                {
                    if (nbs > birthLimit)
                    {
                        newMap[x, y] = true;
                    }
                    else
                    {
                        newMap[x, y] = false;
                    }
                }
            }
        }
        return newMap;
    }

    //Returns the number of cells in a ring around (x,y) that are alive.
    public static int countAliveNeighbours(Boolean[,] map, int x, int y)
    {
        int count = 0;
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                int neighbour_x = x + i;
                int neighbour_y = y + j;
                //If we're looking at the middle point
                if (i == 0 && j == 0)
                {
                    //Do nothing, we don't want to add ourselves in!
                }
                //In case the index we're looking at it off the edge of the map
                else if (neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= width || neighbour_y >= height)
                {
                    count = count + 1;
                }
                //Otherwise, a normal check of the neighbour
                else if (map[neighbour_x, neighbour_y])
                {
                    count = count + 1;
                }
            }
        }
        return count;
    }

    //the function that generates the map
    public static Boolean[,] generateMap()
    {
        //Create a new map
        Boolean[,] cellmap = new Boolean[width, height];
        //Set up the map with random values
        cellmap = initialiseMap();
        //And now run the simulation for a set number of steps
        for (int i = 0; i < numberOfSteps; i++)
        {
            cellmap = doSimulationStep(cellmap);
        }

        return cellmap;
    }

    //generate the map width and length
    public void generateTileMap()
    {
        var map = generateMap();
        tileMap.ClearAllTiles();
        List<Vector3Int> walakableSpaces = new List<Vector3Int>();
        for(int i = 0; i <= width; i++)
        {
            for(int j = 0; j <= height; j++)
            {

                if (i == 0 || j == 0 || i == width || j == height)
                {
                    tileMap.SetTile(new Vector3Int(i, j, 0), emptyTile);
                    tileMap.SetColliderType(new Vector3Int(i, j, 0), Tile.ColliderType.Sprite);
                }
                else
                {
                    int x = i - 1;
                    int y = j - 1;
                    if (map[x, y])
                    {
                        //ground.SetTile(new Vector3Int(x, y, 0), groundTile);
                        tileMap.SetTile(new Vector3Int(i, j, 0), groundTile);
                        tileMap.SetColliderType(new Vector3Int(i, j, 0), Tile.ColliderType.Sprite);
                    }
                    else
                    {
                        tileMap.SetTile(new Vector3Int(i, j, 0), waterTile);
                        walakableSpaces.Add(new Vector3Int(i, j, 0));
                        tileMap.SetColliderType(new Vector3Int(i, j, 0), Tile.ColliderType.None);
                    }
                }
            }
        }

        position = walakableSpaces[new System.Random().Next(walakableSpaces.Count)];
        player.transform.position = new Vector2(position.x, position.y);
    }

    private void Start()
    {
        generateTileMap();
    }
}
