﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace CelularAutomata
{
    class Program
    {
        static int width=60, height=60;
        Boolean[,] cellmap = new Boolean[width, height];

        static int deathLimit = 4;
        static int birthLimit = 5;

        static int numberOfSteps = 3;

        static float chanceToStartAlive = 0.45f;

        public static Boolean[,] initialiseMap()
        {
            Boolean[,] map = new Boolean[width, height];
            Random rng = new Random();

            for (int x = 0; x < width; x++)
            {
                
                for (int y = 0; y < height; y++)
                {
                    double random = rng.NextDouble();
                    if (random < chanceToStartAlive)
                    {
                        map[x,y] = true;
                    }
                }
            }
            return map;
        }

        public static Boolean[,] doSimulationStep(Boolean[,] oldMap)
        {
            Boolean[,] newMap = new Boolean[width,height];
            //Loop over each row and column of the map
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int nbs = countAliveNeighbours(oldMap, x, y);
                    //The new value is based on our simulation rules
                    //First, if a cell is alive but has too few neighbours, kill it.
                    if (oldMap[x,y])
                    {
                        if (nbs < deathLimit)
                        {
                            newMap[x,y] = false;
                        }
                        else {
                            newMap[x,y] = true;
                        }
                    } //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                    else {
                        if (nbs > birthLimit)
                        {
                            newMap[x,y] = true;
                        }
                        else {
                            newMap[x,y] = false;
                        }
                    }
                }
            }
            return newMap;
        }

        //Returns the number of cells in a ring around (x,y) that are alive.
        public static int countAliveNeighbours(Boolean[,] map, int x, int y)
        {
            int count = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int neighbour_x = x + i;
                    int neighbour_y = y + j;
                    //If we're looking at the middle point
                    if (i == 0 && j == 0)
                    {
                        //Do nothing, we don't want to add ourselves in!
                    }
                    //In case the index we're looking at it off the edge of the map
                    else if (neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= width || neighbour_y >= height)
                    {
                        count = count + 1;
                    }
                    //Otherwise, a normal check of the neighbour
                    else if (map[neighbour_x,neighbour_y])
                    {
                        count = count + 1;
                    }
                }
            }
            return count;
        }

        public static Boolean[,] generateMap()
        {
            //Create a new map
            Boolean[,] cellmap = new Boolean[width,height];
            //Set up the map with random values
            cellmap = initialiseMap();
            //And now run the simulation for a set number of steps
            for (int i = 0; i < numberOfSteps; i++)
            {
                cellmap = doSimulationStep(cellmap);
            }

            return cellmap;
        }

        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="image">The image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public static Bitmap resizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.NearestNeighbor;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static Bitmap createImage(Boolean[,] map)
        {
            Bitmap bmp = new Bitmap(width, height);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (map[x, y])
                    {
                        bmp.SetPixel(x, y, Color.FromArgb(0, 0, 0));
                    }
                    else {
                        bmp.SetPixel(x, y, Color.FromArgb(255, 255, 255));
                    }
                    
                }

            }

            bmp = resizeImage(bmp, 512, 512);
            return bmp;
        }

       
        static void Main(string[] args)
        {
            Boolean[,] map = generateMap();

            Bitmap bmp = createImage(map);
            bmp.Save("C:\\Users\\radu\\Documents\\Sync\\Licenta\\chamber.png");
        }
    }
}
